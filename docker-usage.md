## Задание 1
Высоконагруженное монолитное java веб-приложение - физический сервер.

Go-микросервис для генерации отчетов - докер. Go-микросервис можно собрать на основе официального Docker-образа golang, модифицировать его по необходимости, автоматизировать и запустить в самом контейнере.  

Nodejs веб-приложение - докер. 

Мобильное приложение c версиями для Android и iOS - докер.

База данных postgresql используемая, как кэш - докер.

Шина данных на базе Apache Kafka - физический сервер. Важна целостность данных, нельзя ничего терять.

Очередь для Logstash на базе Redis - докер.

Elastic stack для реализации логирования продуктивного веб-приложения - три ноды elasticsearch, два logstash и две ноды kibana - сам стек ELK Stack на физическом сервере, который будет собирать логи из Docker в стек.

Мониторинг-стек на базе prometheus и grafana - докер. Prometheus и grafana полностью совместимы с Docker и доступены на Docker Hub.

Mongodb, как основное хранилище данных для java-приложения - докер.

Jenkins-сервер - докер.

## Задание 2
https://hub.docker.com/repository/docker/marzaeva1993/my_repo

## Задание 3
vagrant@vagrant:~/docker_practice/docker_hm1$ sudo docker ps
CONTAINER ID   IMAGE           COMMAND       CREATED          STATUS          PORTS     NAMES

c970d45649a5   debian:latest   "bash"        10 minutes ago   Up 10 minutes             mydebian

dfcd858549e2   centos:latest   "/bin/bash"   10 minutes ago   Up 10 minutes             mycentos

vagrant@vagrant:~/docker_practice/docker_hm1$ sudo docker exec -ti c970d45649a5 bash 

root@c970d45649a5:/# ls

bin  boot  dev	etc  home  info  lib  lib64  media  mnt  opt  proc  root  run  sbin  srv  sys  tmp  usr  var
root@c970d45649a5:/# cd info/

root@c970d45649a5:/info# ls
file1

root@c970d45649a5:/info# cat file1
This is my first file!



